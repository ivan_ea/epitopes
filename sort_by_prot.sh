#!/usr/bin/env sh

files=$(echo M*.csv)

# Make sure the dummy OFS is not a char in the original part
for f in $files; do
echo "processing $f"
awk -F',' -v OFS='&' -v RS='\r\n' '(FNR == NR){lineno[$2] = NR; next}
     {print lineno[$5], $0;}' proteins.csv unsorted_haplotypes/$f |sort -k 1,1n | cut -d'&' -f2|\
     uniq > "$f"
done
