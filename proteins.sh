#!/usr/bin/env sh

output_file="proteins.csv"
no_seq_file="missing_proteins.csv"
print_e=200

# headers
echo "count,protein_id,aas" > $output_file
echo "count,protein_id" > $no_seq_file

# do initially only 500 for testing 
#all_ids=$(sed -s '1d' *.csv | cut -d',' -f5 | sort | uniq -c | sort -nr |\
#  head -n 500 | awk -v OFS=',' '{print $1,$2}')

all_ids=$(sed -s '1d' *.csv | cut -d',' -f5 | sort | uniq -c | sort -nr |\
  awk -v OFS=',' '($2!=""){print $1,$2}')
num_prots=$(echo $all_ids | wc -w)

# search in Uniprot for the protein sequence associated to the id
if [ -z $1 ]; then
  web="https://3dbionotes.cnb.csic.es/api/info/Uniprot/"
else
  web="https://www.uniprot.org/uniprot/"
fi

echo "There are $num_prots total proteins in all .csv files"
echo "Fetching data from $web (argument $1)"
echo "Printing progress every $print_e"
estimate=$(echo 4*"$num_prots"/1000 | bc)
echo "Estimated duration: ~$estimate minutes"

i=0
for count_id in $all_ids; do
  id=$(echo $count_id | cut -d',' -f2)
  #echo $count_id $id # debug
  if [ -z $1 ]; then
    aa=$(curl -s $web$id)
  else
    aa=$(curl -s "$web$id.fasta" | sed '1d' | tr -d '\n')
  fi

  # Write to file protein sequence if found
  if [ -z $aa ]; then
    #echo "id $id without sequence" # debug
    echo $count_id >> $no_seq_file
  else
    echo "$count_id,$aa" >> $output_file
  fi

  # see progress
  i=$((i+1))
  j=$(expr $i % $print_e)

  if [ $j -eq 0 ]; then
    echo "done $i / $num_prots proteins" 
  fi
done

echo "Results written in: $output_file"
l=$(sed '1d' $no_seq_file | wc -l)
echo "$l protein ids without sequence written in: $no_seq_file"
