#!/usr/bin/env sh

# Clean IEDB query
# a serach results in a zip file with the exported data
# save every exported zip in a different folder (with identifiyng name)

# Actual header="Description,Starting Position,Ending Position,Antigen Accession,Parent Protein Accession"
# simplify the header name for columns
header="epitope,start,end,antigen_id,protein_id"

# For every directory (with a zip file inside)
for d in */ ; do
  name=$(echo $d | cut -d'/' -f 1)
  echo "Processing $name"

  # unzip the exported result of the search (generates big csv)
  unzip "$d"*.zip -d $d

  # keep the headers 
  #awk -F',' -v OFS=',' '
  #(NR==2){print $3,$6,$7,$11,$13}
  #' "$d"*.csv > "$name".csv
  echo $header > "$name".csv

  # keep the relevant fields (and rows)
  awk -F'"' -v OFS=',' '
  (NR>2 && $22!="SRC248339" && $26!=""){
  split($6, a, " "); #gets rid of parentheses in epitope desc
  print a[1], $12, $14, $22, $26}
  ' "$d"*.csv  >> "$name".csv  

  # check how many unidentified proteins in the file (for info)
  awk -F'"' '($22=="SRC248339"){n++}
  END{print "number of unidentified proteins:", n}
  ' "$d"*.csv
  echo "Results written in $name.csv"
done

exit

# If antigen name is SRC248339, it means unidentified protein, so we
# cannot use it for the pipeline if the protein of the epitope is unknown.

# Fields to keep: (column if FS is literally " i.e. a double quote)
# ---------------------------------------------
# $3 Description ($6)	
# $6 Starting Position ($12)
# $7 Ending Position ($14)
# $11 Antigen Accession ($22)
# $13 Parent Protein Accession ($26)

# Some epitope descriptions have the form:
# LPKPPKPVSKMRMATPLLMQA + OX(M11, M13, M19)
# keep the sequence before the space " +"
