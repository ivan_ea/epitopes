#!/usr/bin/env sh

prot_file="proteins.csv"
no_seq_file="missing_proteins.csv"
failed_file="failed_protein.csv"
print_e=10
ini_version=47

web="https://www.uniprot.org/uniprot/"

#webs of interest
#https://www.uniprot.org/uniprot/A0A087X0H2.fasta # returns nothing, but no error
#https://www.uniprot.org/uniprot/A0A087X0H2?version=* # versions
#https://www.uniprot.org/uniprot/A0A075B6Q3.fasta?version=47 # returns thecorrect result

# For every protein id without response

n_ids=$(sed '1d' $no_seq_file | wc -l)
echo "Fetching $n_ids missing proteins" 
i=0

for count_id in $(sed '1d' $no_seq_file); do
  id=$(echo $count_id | cut -d',' -f2)
  counter=$ini_version # decreasing counter to test versions 

  while [ $counter -ge 1 ]; do
    get=$(curl -s "$web$id.fasta?version=$counter" | sed '1d' | tr -d '\n')
    if [ -z $get ]; then
     counter=$((counter - 1))
    else
      echo "$count_id,$get" >> $prot_file
      echo "version needed was $counter" # debug
      break
    fi
  done

  # See progress
  i=$((i+1))
  j=$(expr $i % $print_e)
  if [ $j -eq 0 ]; then
    echo "done $i / $n_ids proteins" 
  fi

  if [ $counter -eq 0 ]; then
    echo $id >> $failed_file
  fi
done
