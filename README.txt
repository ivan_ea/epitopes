1. Human epitope data from https://www.iedb.org/ (zip files)
The haplotypes are: 
- MHC-1 (HLA-A, HLA-B, HLA-C)
- MHC-2 (HLA-DP, HLA-DQ, HLA-DR)

Output: zip files in folders (one for each haplotype)


2. Cleaned to only have the fields: (script clean_data.sh)
# $2 Description	
# $6 Starting Position
# $7 Ending Position
# $11 Antigen Accession
# $13 Parent Protein Accession:

Output: csv files with the names of the folders and 1 epitope per line.
column names and explanation in explain_columns.csv

n of lines    file name
    138298    MHC_1_A.csv
    202300    MHC_1_B.csv
     56404    MHC_1_C.csv
     72059    MHC_2_DP.csv
     29548    MHC_2_DQ.csv
    158949    MHC_2_DR.csv
    657558    total


3. Get the proteins associated to all those epitopes with script: proteins.sh
- Many proteins have more than 1 epitope inside. Only have to store the sequence once.
- There are 23233 unique proteins in all 6 epitope files.
- Protein sequence can be acceced in the urls:
  web="https://3dbionotes.cnb.csic.es/api/info/Uniprot/"+protein_id
  web="https://www.uniprot.org/uniprot/"+protein_id
- Running proteins.sh took 53 minutes.
- Proteins with blank results are stored in missing_proteins.csv  

Output proteins.csv (23132 lines) and missing_proteins.csv (103 lines)

Columns of proteins.csv:
count: number of total epitopes with this parent protein (from all 6 epitope files)
protein_id: Parent Protein Accession
aas: Aminoacid Sequence


4. All proteins with blank results are recovered from older versions of Uniprot
with the script more_proteins.sh
- Newest version tried 47
- Oldest version needed: 1
- Running time: 6 minutes

Output: protein.csv (now with 102 more rows) 23234 lines.

5. Sort epitope files by the most common proteins and remove possible duplicates.
- script sort_by_prot.sh
- Output:
 n lines file name
  129096 MHC_1_A.csv
  190358 MHC_1_B.csv
   53563 MHC_1_C.csv
   71171 MHC_2_DP.csv
   28386 MHC_2_DQ.csv
  145643 MHC_2_DR.csv
  618217 total



SEARCH PARAMETERS (for step 1.)
-------------------------------
# Epitope
- Linear peptide

# Epitope source
- Organism: human (homo sapiens)

# Host
- Human

# Assay
- T Cell
- MHC ligand
- Outcome: Positive
- (Unmark B Cell)

# MHC Restriction
- Find the different haplotypes (6):
- MHC -> MHC molecule
    + class I (human): HLA-A, HLA-B, HLA-C
    + class II (human): HLA-DP, HLA-DQ, HLA-DR

# Disease
- Any
